import IMask from 'imask';
import axios from 'axios';
import serialize from 'form-serialize';

export default () => {
    const metas = document.getElementsByTagName('meta');
    const form = document.querySelector('.main__form');
    const input = document.querySelector('[data-input]');
    const submit = document.querySelector('[data-submit]');

    const getMetaContent = (name) => {
        for (let i = 0; i < metas.length; i++) {
            if (metas[i].getAttribute('name') === name) {
                return metas[i].getAttribute('content');
            }
        }

        return '';
    };

    const param = getMetaContent('csrf-param');
    const token = getMetaContent('csrf-token');

    form.setAttribute('csrf-param', param);
    form.setAttribute('csrf-token', token);

    const phoneMaskOptions = {
        mask: '+{375} (00) 000-00-00',
        lazy: false,
    };
    const codeMaskOptions = {
        mask: '0000',
        lazy: false,
    };
    let mask = new IMask(input, phoneMaskOptions);

    const showHint = (text, timeout = 2000) => {
        const hint = document.createElement('div');
        hint.classList.add('hint');
        hint.innerHTML = text;
        document.body.appendChild(hint);

        if (timeout) {
            setTimeout(() => {
                document.body.removeChild(hint);
            }, timeout);
        }
    };

    const getCode = async () => {
        const str = serialize(form);

        return axios.post('/auth', str)
            .then((responce) => responce)
            .catch((error) => {
                console.error(error);
                showHint('Ошибка получения кода');
            });
    };

    const stepTwo = () => {
        mask.destroy();
        mask = new IMask(input, codeMaskOptions);

        submit.addEventListener('click', () => {
            const { _unmaskedValue: value } = mask;
            const str = serialize(form);

            if (value.length !== 4) {
                showHint('Введите код полностью');
                return;
            }

            axios.post('/auth', str)
                .then((responce) => {
                    switch (responce) {
                    case -1:
                        showHint('Не верный код');
                        break;
                    case -2:
                        showHint('Сессия истекла');
                        setTimeout(() => { window.location.href = '/login'; }, 2000);
                        break;
                    case 1:
                        break;
                    default:
                        showHint('Неизвестная ошибка');
                    }
                });
        });
    };

    const getCodeEvent = () => {
        const { _unmaskedValue: value } = mask;

        if (value.length !== 12) {
            showHint('Введите номер телефона полностью');
            return;
        }

        getCode()
            .catch((error) => {
                console.error(error);
                showHint('Ошибка получения кода');
            });
    };

    submit.addEventListener('click', getCodeEvent);
};
