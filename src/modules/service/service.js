export default () => {
    const serviceToggles = document.querySelectorAll('[data-toggle]');
    const serviceInfos = document.querySelectorAll('[data-info]');

    serviceToggles.forEach((toggleEl) => {
        const id = toggleEl.dataset.toggle;

        toggleEl.addEventListener('click', () => {
            serviceInfos.forEach((infoEl) => {
                if (id === infoEl.dataset.info) infoEl.classList.toggle('closed');
            });
        });
    });
};
