const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PATHS = {
    source: path.join(__dirname, 'src'),
    dist: path.join(__dirname, 'dist'),
};

const cssDev = [
    'style-loader',
    'css-loader',
    'postcss-loader',
    'stylus-loader',
];

const cssProd = [
    {
        loader: MiniCssExtractPlugin.loader,
        options: {
            publicPath: '../',
        },
    },
    'css-loader',
    'postcss-loader',
    'stylus-loader',
];


module.exports = (env, options) => {
    const inProduction = options.mode === 'production';

    return {
        devtool: inProduction ? false : 'cheap-module-source-map',
        entry: {
            index: `${PATHS.source}/static/js/index.js`,
        },
        output: {
            path: PATHS.dist,
            filename: 'js/[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/env'],
                            },
                        },
                    ],
                },
                {
                    test: /\.pug$/,
                    use: [
                        'pug-loader',
                    ],
                },
                {
                    test: /\.styl$/,
                    use: inProduction ? cssProd : cssDev,
                },
                {
                    test: /\.(png|jpe?g|gif|svg|ico)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192,
                                name: 'images/[name].[ext]',
                                fallback: 'file-loader',
                            },
                        },
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                bypassOnDebug: true,
                                mozjpeg: {
                                    progressive: true,
                                    quality: 75,
                                },
                                optipng: {
                                    enabled: false,
                                },
                                pngquant: {
                                    quality: [0.65, 0.9],
                                    speed: 4,
                                },
                                gifsicle: {
                                    interlaced: false,
                                },
                            },
                        },
                    ],
                },
                {
                    test: /\.(webmanifest|xml)$/,
                    use: 'url-loader',
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 500,
                                name: 'fonts/[name].[ext]',
                                fallback: 'file-loader',
                            },
                        },
                    ],
                },
            ],
        },
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: `${PATHS.source}/pages/index.pug`,
                filename: 'index.html',
                hash: true,
            }),
            new HtmlWebpackPlugin({
                template: `${PATHS.source}/pages/services.pug`,
                filename: 'services.html',
                hash: true,
            }),
            new HtmlWebpackPlugin({
                template: `${PATHS.source}/pages/thanks.pug`,
                filename: 'thanks.html',
                hash: true,
            }),
            new HtmlWebpackPlugin({
                template: `${PATHS.source}/pages/auth.pug`,
                filename: 'auth.html',
                hash: true,
            }),
            new MiniCssExtractPlugin({
                filename: 'css/styles.css',
            }),
        ],
        devServer: {
            contentBase: PATHS.dist,
        },
    };
};
